
         fetch('./prodotti.json').then(data => data.json()).then(data => {

            function generateFavouriteButtons(){
                let favouriteBtns = document.querySelectorAll('.favourite')
            
                favouriteBtns.forEach(btn =>{
                    btn.addEventListener('click', function(){
                        btn.classList.toggle('fas')
                        btn.classList.toggle('far')
                        btn.classList.toggle('red')
                    })
            
                })
            
            }

             function populateProd(data){ 
                 
                const prodWrapper = document.querySelector('#prod-wrapper')

                function truncateTitle(title){
                    let splitted = title.split(' ')
                    if (splitted.length > 1){
                        return `${splitted[0]}..`
                    } else {
                        return splitted[0]
                    }


                }



                prodWrapper.innerHTML=``
                
                 
                data.forEach(prod => {
                 let card = document.createElement('div')

                 card.classList.add('col-12', 'col-sm-6', 'col-lg-4', 'mb-4')

                 card.innerHTML= 
                 `
                 <div class="card-product mx-auto" 
                 data-aos="zoom-in-up"
                 data-aos-offset="50"
                 data-aos-delay="0"
                 data-aos-duration="1000"
                 data-aos-easing="ease-in-out"
                
                 data-aos-once="true"
                 
                 >
                            <img class="img-fluid img-card" src="https://picsum.photos/640/360" alt="">
                            <div class="card-product-body tc-white">
                                <div class="d-flex flex-wrap justify-content-around align-items-center">
                                    <h3 class="mb-0" data-bs-toggle="tooltip" data-bs-placement="top" title="${prod.name}">${truncateTitle(prod.name)}</h3>
                                    <i class="far fa-heart favourite fs-3"></i>
                                    <a href="" class="tc-sec w-100 text-center">${prod.category}</a>
                                    
                                </div>
                                <p class="fs-3 text-center">${prod.price}€</p>
                            </div>
                </div>
                 
                 
                 `

                 prodWrapper.appendChild(card)


             })

             generateFavouriteButtons()


             }

             

             function populateCategoryFilter(){

                let categories = Array.from(new Set (data.map(data => data.category)));
                let wrapper = document.querySelector('#wrapper-category-radio')


                categories.forEach((category, i) => {

                    let input = document.createElement('div')
                    input.classList.add('form-check')
                    input.innerHTML =
                    `
                    <input class="form-check-input filter-category" type="radio" name="category-filter" id="flexRadioDefault${i}" data-filter=${category}>
                    <label class="form-check-label red for="flexRadioDefault1">
                     ${category}
                    </label>
                    
                    `
                    wrapper.appendChild(input)


                })

             }

             function filterByCategoryRadio(){
                 let radios = document.querySelectorAll('.filter-category')
                 
                 radios.forEach(radio =>{
                     radio.addEventListener('input', function(){
                         let selected = radio.getAttribute('data-filter')

                         if(selected === 'all'){
                             populateProd(data)
                         }else{
                             let filtered = data.filter(prod => prod.category === selected)

                            populateProd(filtered)
                         }
                         
                         
                     })


                 })




             }


             function filterBySearch(){
                 let input = document.querySelector('#search-input')
                 input.addEventListener('input', function(){
                 let filtered = data.filter(prod => prod.name.toLowerCase().includes(input.value.toLowerCase()))
                 

                 populateProd(filtered)
                 


                })

             }


             function populatePriceFilter(){
                 let minInput = document.querySelector('#min-price-filter')
                 let minLabel = document.querySelector('#min-price-label')

                 let maxInput = document.querySelector('#max-price-filter')
                 let maxLabel = document.querySelector('#max-price-label')

                 let max = data.map(data =>data.price).sort((a, b) => b - a)[0]
                 maxLabel.innerHTML =`${Math.ceil(max)} €`

                //  inizializzo il massimo e l'attributo max
                 minInput.max = max;
                 maxInput.max = max;
                 maxInput.value = max;

                

                 minInput.addEventListener('input', function(e){
                    

                     if((Number(maxInput.value) - 200) <= Number(minInput.value)) {
                         e.preventDefault()
                        minInput.value = Number(maxInput.value) - 200 
                    } 
                    
                    minLabel.innerHTML = `${minInput.value} €`
                 })

                 maxInput.addEventListener('input', function(e){
                     
                     
                     if((Number(maxInput.value) - 200) <= Number(minInput.value)) {
                         e.preventDefault()
                         maxInput.value = Number(minInput.value) + 200 
                     }
                     
                     maxLabel.innerHTML = `${maxInput.value} €`


                 })


                

                    
                }
                
                function filterByPrice(){
                    let minInput = document.querySelector('#min-price-filter')
                    let maxInput = document.querySelector('#max-price-filter')

                    minInput.addEventListener('change', function(){
                        let filtered = data.filter(data => Number(data.price) > Number(minInput.value) && Number(data.price) <= Number(maxInput.value) + 1)
                        populateProd(filtered)


                    })

                    maxInput.addEventListener('change', function(){
                        let filtered = data.filter(data => Number(data.price) > Number(minInput.value) && Number(data.price) <= Number(maxInput.value) + 1)
                        populateProd(filtered)


                    })





             }
             



 
populateProd(data)
populatePriceFilter()
populateCategoryFilter()
filterByCategoryRadio()
filterBySearch()
filterByPrice()   



         } )



        

