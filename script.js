// menu show

const showMenu = (toggleId, navId) =>{
    const toggle = document.getElementById(toggleId),
    nav = document.getElementById(navId)

    if(toggle && nav){
        toggle.addEventListener('click', ()=>{
            nav.classList.toggle('show')
        })
    }

}

// tooltip
let tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
let tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl)
})




showMenu('nav-toggle', 'nav-menu')

// remove menu

const navLink = document.querySelectorAll('.nav__link')
function linkAction(){
    // active link
    navLink.forEach(n => n.classList.remove('active'))
    this.classList.add('active')

    // remove menu mobile
    const navMenu = document.getElementById('nav-menu')
    navMenu.classList.remove('show')


}

navLink.forEach(n=>n.addEventListener('click', linkAction))



function generateFavouriteButtons(){
    let favouriteBtns = document.querySelectorAll('.favourite')

    favouriteBtns.forEach(btn =>{
        btn.addEventListener('click', function(){
            btn.classList.toggle('fas')
            btn.classList.toggle('far')
            btn.classList.toggle('red')
        })

    })

}

generateFavouriteButtons()